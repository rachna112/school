<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->database();
		$this->load->model('student_model');
		$this->load->library('session');
	}

	public function index()
	{
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}
	public function signup()
	{
		$table_name = 'student_signup';
		$request = $this->input->server('REQUEST_METHOD');
		if($request == 'POST'){
			$insertArray = array(
				'StudentName'        => $this->input->post('StudentName'),
				'StudentRoll_Number' => $this->input->post('StudentRoll_Number'),
				'StudentSection'     => $this->input->post('StudentSection'),
				'StudentBranch'      => $this->input->post('StudentBranch'),
				'StudentSex'         => $this->input->post('StudentSex'),
				'StudentContact_No'  => $this->input->post('StudentContact_No'),
				'StudentAddress'     => $this->input->post('StudentAddress'),
				'StudentAgree'       => $this->input->post('StudentAgree')
			);
			// print_r($insertArray);
			//print_r($_POST);
			//die;
			$StudentId = $this->input->post('StudentId');
			// echo $StudentId.'ghg';die;
			if($StudentId){
				$this->student_model->update_student_signup($table_name,$insertArray,$StudentId);
			}
			else{
				$this->student_model->insert_student_signup($table_name,$insertArray);
			}
			
			redirect(base_url().'Student/manage_studentData');
		}
		
	}
	public function manage_studentData()
	{
		$data['row']=$this->student_model->fetch_studentData();
		// echo "<pre>";
		// print_r($data['row']);
		// exit;
		$this->load->view('manage_student_data',$data);
	}
	public function edit()
	{
		//echo $view_id = $this->uri->segment(4);
		//echo $edit_id = $this->uri->segment(3);
		$view_id = $this->uri->segment(4);
		$edit_id = $this->uri->segment(3);
		
		
		if($view_id){
		$result1['true'] = "view";
		$result1['row'] = $this->student_model->edit_studentData('student_signup',$view_id);	
		
		// echo "<pre>";
		// print_r($result1['row']);
		// print_r($result1['true']);
		// exit;
		$this->load->view('index',$result1);
		}
		else if($edit_id){
		$result1['row'] = $this->student_model->edit_studentData('student_signup',$edit_id);
		$this->load->view('index',$result1);
		}
		
	}

	public function delete()
	{
		//echo $delete_id = $this->uri->segment(3);die;
		$delete_id = $this->uri->segment(3);
		$result1['result'] = $this->student_model->delete_studentData('student_signup',$delete_id);
		redirect("Student/manage_studentData");
	}

	public function data() 
		{
           // "SELECT * FROM `tblengagement` where Engagement_Id=1"
	        $query = "SELECT * FROM tblengagement a 
			inner join tblengagement_milestones b 
			on a.Engagement_Id = b.Engagement_Id
			where a.Win_Date + INTERVAL b.Due_Months MONTH > CURRENT_DATE
			and b.Is_Invoice_Raised != 1 ";

			$content['Milestone_list'] = $this->student_model->query_data($query);
			// echo "<pre>";
			// print_r($content['Milestone_list']);
			// exit;

			$this->load->view('data_view',$content);
		}

	
}
