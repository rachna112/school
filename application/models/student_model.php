<?php
class student_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
    //$this->load->library('session');
  }

  public function insert_student_signup($table_name,$data)
  {
    //echo $table_name;
    //print_r($data);
    //die("here code die");
    $this->db->insert($table_name,$data);
  }
  public function update_student_signup($table_name,$data,$id){
    $this->db->where('StudentId', $id);
    $this->db->update($table_name, $data);
    return;
  }
  public function fetch_studentData()
  {
    //echo $table_name;
    $this->db->select('*');
    $this->db->from('student_signup');
    $query = $this->db->get();
    if ($query->num_rows() > 0 )
    {
        //$result = $query->row_array();
        //$result = $query->result_array();
      $result = $query->result_object();
        return $result;
    }
    else{
      return false;
    }
  }
  public function edit_studentData($table_name,$id)
  {
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->where('StudentId',$id);
    $query = $this->db->get();

    if ($query->num_rows() > 0 )
    {
        //$result = $query->row_array();
        $result = $query->result_array();
        return $result;
    }
    else{
      return false;
    } 
    // print_r($result);
    // die("mlvkfd");
  }

  public function delete_studentData($table_name,$id)
  {
    $this->db->where('StudentId', $id);
    $this->db->delete($table_name);
    return;
  }

  public function query_data($sql){
    $query = $this->db->query($sql);
    return $query->result();
  }

}